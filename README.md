# Bank account example app
[![Code coverage percentage](https://projektorlive.herokuapp.com/repo/craigatk/bank-account-example/badge/coverage)](https://projektorlive.herokuapp.com/repository/craigatk/bank-account-example/coverage)

In case of database change:
- stop the docker compose task if it is still running
- add the SQL migration file `V2_SomeDescription.sql`
- run command `./gradlew generateJooq`

To run the unit tests: 
- run `docker compose up`
- run `./gradlew clean test`

Example application with the following tech stack:

* [Kotlin](https://kotlinlang.org/)
* [ktor](https://ktor.io/)
* [jOOQ](https://www.jooq.org/)
* [Strikt](https://strikt.io/)
* [k6](https://k6.io/)

Blog post detailing integrating database access and management with Flyway and jOOQ into a Kotlin Ktor app: https://www.atkinsondev.com/post/database-jooq-flyway-ktor/
